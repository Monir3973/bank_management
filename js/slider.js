﻿$("#slideshows img:gt(0)").hide();
$('#slideshows').css('position','relative');
$('#slideshows img').css('position','absolute');
setInterval(function() {
  $('#slideshows img:first')
    .fadeOut(1000)
    .next()
    .fadeIn(1000)
    .end()
    .appendTo('#slideshows');
},  3000);